#!/usr/bin/env python
import os, os.path
import sys
import struct
import gzip, bz2
import hashlib

def getStreamDetails(f):
        size = 0
        sha = hashlib.sha1()
        while True:
                buf = f.read(1024)
                if not buf: break
                sha.update(buf)
                size += len(buf)
        return (size, sha.hexdigest())

def getBzip2Details(path):
        with bz2.BZ2File(path, 'rb') as f:
                return getStreamDetails(f)


def getGzipDetails(path):
        with gzip.GzipFile(path, 'rb') as f:
                return getStreamDetails(f)

def getFileDetails(path):
        return getStreamDetails(open(path, 'rb'))

def main(args):
        if len(args) != 1:
                print "Please provide path to manifest"
                return

        path = args[0]

        print "R\t%s" % path
        os.chdir(path)
        for root, dirs, files in os.walk("./", followlinks=True):
                for fn in files:
                        if fn == ".pure":
                                print "P\t%s" % (root[2:])
                        else:
                                fullpath = os.path.join(root[2:], fn)
                                stat = os.stat(fullpath)
                                filesize = stat.st_size
                                compress = ""
                                sha = ''

                                if fullpath[-3:] == ".gz":
                                        filesize, sha = getGzipDetails(fullpath)
                                        fullpath = fullpath[:-3]
                                        compress = "gz"
                                elif fullpath[-4:] == ".bz2":
                                        filesize, sha = getBzip2Details(fullpath)
                                        fullpath = fullpath[:-4]
                                        compress = "bz2"
                                else:
                                        compress = "raw"
                                        filesize, sha = getFileDetails(fullpath)

                                print "A\t%s\t%d\t%d\t%s\t%s" % (fullpath, filesize, stat.st_mtime, compress, sha)


if __name__ == "__main__":
        main(sys.argv[1:])

