﻿using armasync.lib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace armasync
{
    public class SelfUpdater
    {
        private readonly string _mirror;

        public SelfUpdater(string mirror)
        {
            this._mirror = mirror;
        }

        public string VersionFileUrl
        {
            get
            {
                return string.Format("{0}/launcher/version", _mirror);
            }
        }

        public string UpdateWebUrl
        {
            get
            {
                return _mirror;
            }
        }

        public string LauncherExeUrl
        {
            get
            {
                return string.Format("{0}/launcher/zdyn-arma.exe", _mirror);
            }
        }

        private Version _currentVersion;
        public Version CurrentVersion
        {
            get
            {
                if (_currentVersion == null)
                {
                    _currentVersion = Assembly.GetExecutingAssembly().GetName().Version;
                }
                return _currentVersion;
            }
        }

        public bool CheckForUpdate()
        {
            Console.WriteLine("Checking for launcher update...");
            var downloaded = new HttpDownloader();
            string versionString = downloaded.DownloadText(this.VersionFileUrl);

            if (!string.IsNullOrEmpty(versionString))
            {
                try
                {
                    var v = new Version(versionString);
                    return v > this.CurrentVersion;
                }
                catch
                {
                    Console.WriteLine("Error parsing version");
                }
            }

            return false;
        }

        public bool TryAutoUpdate()
        {
            string tempFile = Path.GetTempFileName();
            var downloader = new HttpDownloader();

            Console.Write("Downloading launcher update... ");

            if (!downloader.Download(this.LauncherExeUrl, tempFile))
            {
                Console.WriteLine("Failed");
                return false;
            }
            else
            {
                Console.WriteLine("OK");
            }
            
            //Move around the temp file to replace the current one, and restart
            try
            {
                string currExe = Assembly.GetEntryAssembly().Location;
                File.Move(currExe, currExe + ".old");
                File.Move(tempFile, currExe);

                Console.WriteLine("Update complete, restarting");

                string args = string.Join(" ", Environment.GetCommandLineArgs());
                Process.Start(currExe, args);
                Environment.Exit(0);
                return true; //Won't ever really get here
            }
            catch
            {
                return false;
            }
        }

        public void TryFinishUpdate()
        {
            string oldExe = Assembly.GetEntryAssembly().Location + ".old";
            if (File.Exists(oldExe))
            {
                Thread.Sleep(500); //Give a sec for it to shut down
                try
                {
                    File.Delete(oldExe);
                }
                catch { }
            }
        }
    }
}
