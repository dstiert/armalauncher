using armasync.lib;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace armasync
{
    class MainClass
    {
        [STAThread]
        public static void Main (string[] args)
        {
            if (Arguments.Instance.ShowHelp)
            {
                Arguments.Instance.WriteHelp(Console.Out);
                Environment.Exit(0);
            }

            CheckForUpdate();

            if (Arguments.Instance.UpdateOnly)
                Environment.Exit(0);

            string path = DiscoverPath ();
            if (string.IsNullOrEmpty(path))
            {
                Console.Error.WriteLine ("Unable to discover path. Please enter it in the CLI");
                Environment.Exit (1);
            }

            if (!Config.ValidatePath(path))
            {
                Console.Error.WriteLine("Unable to find arma in path");
                Environment.Exit(1);
            }

            AppSettings.Instance.GamePath = path;
            AppSettings.Instance.Save();

            var syncer = new Syncer (Config.MIRROR, Arguments.Instance.ManifestNameOverride ?? Config.MIRROR_MANIFEST,  path);

            DateTime updateStart = DateTime.UtcNow;
            Console.WriteLine("Validating mods...");
            if (!syncer.Update())
            {
                Console.Error.WriteLine ("Failed to update");
                Environment.Exit (1);
            }

            if (Platform.Instance.IsUnix || Arguments.Instance.NoLaunch)
                Environment.Exit(0); //Don't launch in unix

            //Confirmation message if update takes more than 10 seconds
            if (DateTime.UtcNow - updateStart > TimeSpan.FromSeconds(10))
            {
                //We took more than 10 seconds to update, so we should confirm the launch
                var confirm = MessageBox.Show("Update complate.  Would you like to launch Arma now?", "Launch?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirm == DialogResult.No)
                    Environment.Exit(0);
            }

#if !DEBUG
            Launch(path);
#endif
        }

        private static void Launch(string path)
        {
            Console.WriteLine("Launching!");
            Environment.CurrentDirectory = path;
            string launcher = Path.Combine(path, Config.LAUNCH_CMD);
            Process.Start(launcher);
        }

        private static string DiscoverPath()
        {
            if (!string.IsNullOrEmpty(Arguments.Instance.Target))
                return Arguments.Instance.Target;

            if (!string.IsNullOrEmpty(AppSettings.Instance.GamePath))
                return AppSettings.Instance.GamePath;

            foreach (var path in Config.SEARCH_PATHS)
            {
                if (Directory.Exists(path))
                    return path;
            }

            //Current path
            if (Config.ValidatePath(Environment.CurrentDirectory))
                return Environment.CurrentDirectory;

            //Don't continue on unix for folder browser
            if (Platform.Instance.IsUnix)
            {
                return null;
            }

            Console.WriteLine("Unable to find folder, please navigate to Arma3 folder");
            var browser = new FolderBrowserDialog();
            browser.Description = "Unable to find Arma 3 automatically. Choose the folder where arma3.exe exists.  This may be your steam path or another drive.";
            var result = browser.ShowDialog();

            if (result == DialogResult.OK)
            {
                return browser.SelectedPath;
            }

            return null;
        }


        private static void CheckForUpdate()
        {
            var updater = new SelfUpdater(Config.MIRROR);

            updater.TryFinishUpdate();
            if (updater.CheckForUpdate())
            {
                if (!updater.TryAutoUpdate())
                {
                    //Couldn't auto-update, fallback
                    if (Platform.Instance.IsUnix)
                    {
                        Console.WriteLine("Update available.  You can retrieve it at {0}", updater.LauncherExeUrl);
                    }
                    else
                    {
                        var result = MessageBox.Show("An update is available for the launcher.  Would you like to download it now?", "Update?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            Process.Start(updater.UpdateWebUrl);

                            Environment.Exit(0);
                        }
                    }
                }
            }
        }
    }
}
