using System;
using System.Net;
using System.IO;
using System.Text;

namespace armasync.lib
{
    public class HttpDownloader
    {
        private static readonly TimeSpan UPDATE_FREQ = TimeSpan.FromSeconds(0.1);

        public delegate void ProgressDelegate(string url, long received, long total, double speed);

        public event ProgressDelegate UpdateProgress;

        private Exception _lastError;
        public Exception LastError
        {
            get { return _lastError; }
        }

        public HttpDownloader ()
        {
        }

        public bool Download(string url, Stream ostream)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 30000;
                request.AllowWriteStreamBuffering = false;
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                var response = request.GetResponse();
                long contentLength = response.ContentLength;

                var responseStream = response.GetResponseStream();

                DateTime lastUpdate = DateTime.UtcNow;
                int read;
                long totalBytes = 0;
                long lastUpdateBytes = 0;
                double lastSpeed = 0.0;
                byte[] buffer = new byte[8192];
                while((read = responseStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ostream.Write(buffer, 0, read);
                    totalBytes += read;

                    var now = DateTime.UtcNow;
                    if (now - lastUpdate > UPDATE_FREQ)
                    {
                        lastSpeed = (lastSpeed + (totalBytes - lastUpdateBytes) / (now - lastUpdate).TotalSeconds) / 2.0; //running average
                        OnUpdateProgress(url, totalBytes, contentLength, lastSpeed);
                        lastUpdateBytes = totalBytes;
                        lastUpdate = now;
                    }
                }

                responseStream.Dispose();

            }
            catch(Exception e)
            {
                _lastError = e;
                return false;
            }
            return true;
        }

        public bool Download(string url, string toFile)
        {
            if (File.Exists(toFile))
                File.Delete(toFile);

            bool ret = false;
            using(var stream = File.OpenWrite(toFile))
            {
                ret = this.Download (url, stream);
            }

            if (!ret)
                File.Delete(toFile);
            return ret;
        }

        public string DownloadText(string url)
        {
            using(var ms = new MemoryStream())
            {
                if (!this.Download(url, ms))
                {
                    return null;
                }

                return Encoding.UTF8.GetString (ms.ToArray ());
            }
        }

        protected virtual void OnUpdateProgress(string filename, long received, long total, double speed)
        {
            if (this.UpdateProgress != null)
            {
                try
                {
                    this.UpdateProgress(filename, received, total, speed);
                }
                catch{}
            }
        }

    }
}

